package com.company.employee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.company.employee.entity.Genders;
import com.company.employee.entity.Jobs;
import com.company.employee.repository.IGenderRepo;
import com.company.employee.repository.IJobRepo;

@SpringBootApplication
public class EmployeeApplication implements CommandLineRunner{

	@Autowired
	IJobRepo jobRepo;

	@Autowired
	IGenderRepo genderRepo;

	public static void main(String[] args) {
		SpringApplication.run(EmployeeApplication.class, args);	
	}

	@Override
	public void run(String... args) throws Exception {
		try {

			List<String> jobs =  	Arrays.asList("Maestro", "Doctor", "Contador", "Abogado", "Desarrollador");
			List<Double> salaries = Arrays.asList(12.5, 17.8,15.3, 11.9,16.4);

			for (int i = 0; i<jobs.size(); i++) {
				Jobs newJob = new Jobs();
				newJob.setName(jobs.get(i));
				newJob.setSalary(salaries.get(i));

				jobRepo.save(newJob);
			}

			Genders genderH = new Genders();		
			genderH.setName("Hombre");
			genderRepo.save(genderH);

			Genders genderM = new Genders();		
			genderM.setName("Mujer");
			genderRepo.save(genderM);

		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
}
