package com.company.employee.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
@Data
public class Employees {

	/** ID. */
	@Id
	@GeneratedValue
	private int employeeId;

	/** Genéro. */
	@ManyToOne
	@JoinColumn(name="gender_id", insertable = false, updatable = false)
	private Genders gender;

	@Column(name = "gender_id")
	private Integer genderId;

	/** Trabajo. */
	@ManyToOne
	@JoinColumn(name="job_id", insertable = false, updatable = false)
	private Jobs job;

	@Column(name="job_id")
	private Integer jobId;

	/** Nombre. */
	@Column
	private String name;

	/** Apellido. */
	@Column
	private String lastName;

	/** Fecha de nacimiento. */
	@Column
	private Date birthDate;
}
