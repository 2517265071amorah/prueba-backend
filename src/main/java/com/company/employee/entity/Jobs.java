package com.company.employee.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
@Data
public class Jobs {

	/** ID. */
	@Id
	@GeneratedValue
	private int id;

	/** Nombre. */
	@Column
	private String name;

	/** Sueldo. */
	@Column
	private double salary;
}
