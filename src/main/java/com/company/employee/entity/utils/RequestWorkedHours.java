package com.company.employee.entity.utils;

import java.util.Date;

import lombok.Data;

@Data
public class RequestWorkedHours {

	private int employeeId;
	private Date startDate;
	private Date endDate;
}
