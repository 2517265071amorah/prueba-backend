package com.company.employee.entity.utils;

import com.company.employee.entity.Employees;

import lombok.Data;

import java.util.List;

@Data
public class ResponseEmployeeJob {

	private List<Employees> employees;
	private boolean success;

	public ResponseEmployeeJob(List<Employees> employees, boolean success) {
		super();
		this.employees = employees;
		this.success = success;
	}


}
