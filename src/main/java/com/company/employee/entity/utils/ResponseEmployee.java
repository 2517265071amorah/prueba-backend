package com.company.employee.entity.utils;

import lombok.Data;

@Data
public class ResponseEmployee {
	
	private Integer id;
	private boolean success;
	
	public ResponseEmployee(Integer id, boolean success) {
		this.id= id;
		this.success = success;
	}

}
