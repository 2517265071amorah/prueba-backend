package com.company.employee.entity.utils;

import lombok.Data;

@Data
public class ResponseEmployeeHours {

	private Integer totalWorkedHours;
	private boolean success;

	public ResponseEmployeeHours(Integer totalWorkedHours, boolean success) {
		super();
		this.totalWorkedHours = totalWorkedHours;
		this.success = success;
	}
}
