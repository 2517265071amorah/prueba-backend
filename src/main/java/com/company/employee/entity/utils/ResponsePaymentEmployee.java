package com.company.employee.entity.utils;

import lombok.Data;

@Data
public class ResponsePaymentEmployee {

	private Double payment;
	private boolean success;

	public ResponsePaymentEmployee(Double payment, boolean success) {
		super();
		this.payment = payment;
		this.success = success;
	}

}
