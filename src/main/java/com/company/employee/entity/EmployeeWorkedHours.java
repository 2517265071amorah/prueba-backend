package com.company.employee.entity;

import javax.persistence.Table;

import lombok.Data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table
@Data
public class EmployeeWorkedHours {

	/** ID. */
	@Id
	@GeneratedValue
	private int id;

	/** Empleado. */
	@ManyToOne
	@JoinColumn(name = "employee_id", insertable = false, updatable = false)
	private Employees employee;

	/** Empleado. */
	@Column(name = "employee_id")
	private Integer employeeId;

	/** Horas trabajadas. */
	@Column
	private int workedHours;

	/** Fecha de trabajo. */
	@Column
	private Date workedDate;
}
