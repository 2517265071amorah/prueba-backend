package com.company.employee.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.employee.entity.EmployeeWorkedHours;
import com.company.employee.entity.Employees;
import com.company.employee.entity.Genders;
import com.company.employee.entity.Jobs;
import com.company.employee.entity.utils.RequestWorkedHours;
import com.company.employee.entity.utils.ResponseEmployee;
import com.company.employee.entity.utils.ResponseEmployeeHours;
import com.company.employee.entity.utils.ResponseEmployeeJob;
import com.company.employee.entity.utils.ResponsePaymentEmployee;
import com.company.employee.repository.IEmployeeRepo;
import com.company.employee.repository.IEmployeeWorkedHoursRepo;
import com.company.employee.repository.IGenderRepo;
import com.company.employee.repository.IJobRepo;

@RestController
@RequestMapping(path = "employee")
public class EmployeeController {

	@Autowired
	private IEmployeeRepo employeeRepo;

	@Autowired
	private IGenderRepo genderRepo;

	@Autowired
	private IJobRepo jobRepo;

	@Autowired
	private IEmployeeWorkedHoursRepo workedHoursRepo;

	/**
	 * Ejercicio 1
	 * @param employee
	 * @return
	 */
	@PostMapping("")
	public ResponseEmployee createEmployee(@RequestBody Employees employee) {

		// Validar que el nombre no este registrado 
		Employees employeeName = 
				employeeRepo.findEmployeesByNameAndLastName(employee.getName(), employee.getLastName());

		// Validar edad
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
		String fechaInicioStr = dateFormat.format(employee.getBirthDate());  

		String fechaFinStr = dateFormat.format(new Date());  

		LocalDate fechaInicio = LocalDate.parse(fechaInicioStr, fmt);
		LocalDate fechaFin = LocalDate.parse(fechaFinStr, fmt);

		Period periodo = Period.between(fechaInicio, fechaFin);

		// Validar si existen el puesto y genero
		Optional<Genders> gender = genderRepo.findById(employee.getGenderId());
		Optional<Jobs> job = jobRepo.findById(employee.getJobId());

		// Validaciones
		if ((periodo.getYears() < 18) || employeeName != null || gender.isEmpty() || job.isEmpty()) {
			// Candidato no valido
			return new ResponseEmployee(null, false);
		}

		employee = employeeRepo.save(employee);
		return new ResponseEmployee(employee.getEmployeeId(), true);
	}

	/**
	 * Ejercicio 2
	 * @param hours
	 * @return
	 */
	@PostMapping("worked-hours")
	public ResponseEmployee saveWorkedHours(@RequestBody EmployeeWorkedHours hours) {

		// Validar que exista
		Optional<Employees> employee = employeeRepo.findById(hours.getEmployeeId());

		EmployeeWorkedHours employeeDate = 
				workedHoursRepo.findEmployeeWorkedHoursByemployeeIdAndWorkedDate(hours.getEmployeeId(), hours.getWorkedDate());
		// Validaciones
		if (employee.isEmpty() || hours.getWorkedHours() > 20 
				|| hours.getWorkedDate().after(new Date()) || employeeDate != null) {
			return new ResponseEmployee(null, false);
		}

		hours = workedHoursRepo.save(hours);

		return new ResponseEmployee(hours.getId(), true);
	}

	/**
	 * Ejercicio 3
	 * @param job
	 * @return
	 */
	@GetMapping("job")
	public ResponseEmployeeJob getEmployeesByJob(@RequestBody Jobs job){

		Optional<Jobs> jobBD = jobRepo.findById(job.getId());

		if (jobBD.isEmpty()) {
			return new ResponseEmployeeJob(null, false);
		}
		return new ResponseEmployeeJob(employeeRepo.findByJobId(job.getId()), true);
	}

	/**
	 * Ejercicio 4
	 * @param requestWorkedHours
	 * @return
	 */
	@GetMapping("worked-hours")
	public ResponseEmployeeHours getWorkedHours(@RequestBody RequestWorkedHours requestWorkedHours) {

		// Horas trabajadas
		int hours = 0;

		// El empleado exista
		Optional<Employees> employee = employeeRepo.findById(requestWorkedHours.getEmployeeId());
		// Validaciones
		if (employee.isEmpty() || (requestWorkedHours.getStartDate().after(requestWorkedHours.getEndDate()))) {
			return new ResponseEmployeeHours(null, false);
		}

		// Calcular horas trabajadas
		List<EmployeeWorkedHours> journaly = 
				workedHoursRepo.findByEmployeeIdAndWorkedDateBetween(
						requestWorkedHours.getEmployeeId(), requestWorkedHours.getStartDate(), requestWorkedHours.getEndDate());

		for (int i = 0; i < journaly.size(); i++) {
			hours += journaly.get(i).getWorkedHours();
		}
		return new ResponseEmployeeHours(hours, true);
	}

	/**
	 * Ejercicio 5
	 * @param requestWorkedHours
	 * @return
	 */
	@GetMapping("salary")
	public ResponsePaymentEmployee getSalary(@RequestBody RequestWorkedHours requestWorkedHours) {

		ResponseEmployeeHours response = getWorkedHours(requestWorkedHours);

		// Saber sueldo
		if (response.isSuccess()) {
			Optional<Employees> employee = employeeRepo.findById(requestWorkedHours.getEmployeeId());

			double salary = employee.get().getJob().getSalary() * response.getTotalWorkedHours();

			return new ResponsePaymentEmployee(salary, true);
		}else {
			return new ResponsePaymentEmployee(null, false);	
		}

	}

}
