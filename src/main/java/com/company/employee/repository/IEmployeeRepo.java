package com.company.employee.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.employee.entity.Employees;

@Repository
public interface IEmployeeRepo extends JpaRepository<Employees, Integer>{

	Employees findEmployeesByNameAndLastName(String name, String lastName);

	List<Employees> findByJobId(int id);

}
