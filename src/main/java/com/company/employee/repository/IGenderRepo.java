package com.company.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.employee.entity.Genders;

@Repository
public interface IGenderRepo extends JpaRepository<Genders, Integer>{

}
