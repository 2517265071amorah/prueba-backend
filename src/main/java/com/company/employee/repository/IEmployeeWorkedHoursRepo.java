package com.company.employee.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.employee.entity.EmployeeWorkedHours;

@Repository
public interface IEmployeeWorkedHoursRepo extends JpaRepository<EmployeeWorkedHours, Integer>{

	EmployeeWorkedHours findEmployeeWorkedHoursByemployeeIdAndWorkedDate(Integer employeeId, Date workedDate);

	List<EmployeeWorkedHours> findByEmployeeIdAndWorkedDateBetween(int employeeId, Date startDate, Date endDate);

}
