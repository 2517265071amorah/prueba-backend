package com.company.employee.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.company.employee.entity.Jobs;

@Repository
public interface IJobRepo extends JpaRepository<Jobs, Integer>{

}
